import {withStyles, InputBase} from '@material-ui/core'

export const StylesSelectorInput = withStyles(theme => ({
  input: {
    borderRadius: 5,
    position: 'relative',
    backgroundColor: 'none',
    border: '1px solid #009DE0',
    fontWeight: 600,
    fontSize: 14,
    color: '#009DE0',
    padding: '6px 26px 7px 9px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      backgroundColor: 'none',
    },
  },
}))(InputBase)
