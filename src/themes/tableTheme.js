import {createTheme} from '@material-ui/core/styles'
export const inputTheme = createTheme({
  overrides: {
    MuiInput: {
      underline: {
        '&&&:before': {
          borderBottom: 'none',
        },
        '&&:after': {
          borderBottom: 'none',
        },
      },
    },
    MuiInputBase: {
      input: {
        height: 'none',
        padding: '6px 0',
      },
    },
    MuiTableCell: {
      root: {
        borderBottom: '1px solid #009DE0',
      },
    },
    MuiSvgIcon: {
      root: {
        color: '#009DE0',
      },
    },
    MuiOutlinedInput: {
      notchedOutline: {
        border: 'none',
      },
    },
    MuiIconButton: {
      root: {
        paddingLeft: 0,
      },
    },
    MuiTouchRipple: {
      root: {
        color: 'none',
      },
    },
  },
})
