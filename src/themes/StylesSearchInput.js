import {withStyles, InputBase} from '@material-ui/core'

export const StylesSearchInput = withStyles(theme => ({
  root: {
    border: 'none',
    outline: 'none',
    background: '#fff',
    width: '100%',
    fontSize: 18,
    height: 'auto',
    color: '#000',
  },
  input: {
    height: 'auto',
  },
  adornedStart: {
    paddingLeft: 24,
    color: '#B5B5C3',
  },
}))(InputBase)
