import {createTheme} from '@material-ui/core/styles'
export const appTheme = createTheme({
  palette: {
    primary: {
      main: '#009DE0',
    },
    secondary: {
      main: '#B5B5C3',
    },
  },
  spacing: [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50],
  mixins: {
    toolbar: {
      minHeight: 54,
      '@media (min-width:600px)': {
        minHeight: 54,
      },
      '@media (min-width:0px)': {
        minHeight: 54,
      },
    },
  },
  overrides: {
    'a:hover': {
      textDecoration: 'none',
    },
    MuiCard: {
      root: {
        width: '100%',
      },
    },
    MuiIconButton: {
      root: {
        color: '#009DE0',
      },
    },
    MuiFormControl: {
      root: {
        width: '100%',
      },
      marginNormal: {
        marginTop: 0,
        marginBottom: 0,
      },
    },
    MuiPaper: {
      elevation1: {
        boxShadow: 'none',
      },
      rounded: {
        borderRadius: 5,
        boxShadow: 'none',
      },
    },
    MuiButton: {
      label: {
        textTransform: 'none',
      },
      contained: {
        boxShadow: 'none',
      },
    },

    MuiInput: {
      underline: {
        '&&&:before': {
          borderBottom: 'none',
        },
        '&&:after': {
          borderBottom: 'none',
        },
      },
      formControl: {
        border: '1px solid #009DE0',
        borderRadius: 5,
        margin: 0,
      },
    },
    MuiInputBase: {
      root: {
        'label + &': {
          marginTop: 15,
        },
      },
      input: {
        borderRadius: 5,
        position: 'relative',
        fontSize: 14,
        padding: '10px 23px',
        fontFamily: [
          '-apple-system',
          'BlinkMacSystemFont',
          '"Segoe UI"',
          'Roboto',
          '"Helvetica Neue"',
          'Arial',
          'sans-serif',
          '"Apple Color Emoji"',
          '"Segoe UI Emoji"',
          '"Segoe UI Symbol"',
        ].join(','),
      },
    },
    MuiInputLabel: {
      formControl: {
        transform: 'none',
        fontSize: 14,
        fontWeight: 600,
        color: '#000',
      },
      shrink: {
        transform: 'none',
      },
    },
    MuiSelect: {
      selectMenu: {
        height: 0,
      },
      icon: {
        color: '#009DE0',
      },
      select: {
        '&&:focus': {
          backgroundColor: '#fff',
        },
      },
    },
    MuiList: {
      padding: {
        paddingTop: 0,
        paddingBottom: 0,
      },
    },

    MuiBackdrop: {
      root: {
        backgroundColor: 'rgba(4, 62, 104, .7)',
      },
    },
  },
})
