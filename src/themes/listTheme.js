import {createTheme} from '@material-ui/core/styles'

export const listTheme = createTheme({
  palette: {
    primary: {
      main: '#009DE0',
    },
    secondary: {
      main: '#B5B5C3',
    },
    spacing: [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50],
  },
  overrides: {
    MuiButton: {
      label: {
        textTransform: 'capitalize',
      },
      contained: {
        boxShadow: 'none',
      },
    },

    MuiInput: {
      underline: {
        '&&&:before': {
          borderBottom: 'none',
        },
        '&&:after': {
          borderBottom: 'none',
        },
      },
      formControl: {
        border: '1px solid #009DE0',
        borderRadius: 5,
        margin: 0,
      },
    },
    MuiInputBase: {
      input: {
        borderRadius: 5,
        position: 'relative',
        fontSize: 14,
        padding: '10px 23px',
        fontFamily: [
          '-apple-system',
          'BlinkMacSystemFont',
          '"Segoe UI"',
          'Roboto',
          '"Helvetica Neue"',
          'Arial',
          'sans-serif',
          '"Apple Color Emoji"',
          '"Segoe UI Emoji"',
          '"Segoe UI Symbol"',
        ].join(','),
      },
    },
    MuiBackdrop: {
      root: {
        backgroundColor: 'rgba(4, 62, 104, .7)',
      },
    },
    MuiSelect: {
      selectMenu: {
        height: 0,
      },
      icon: {
        color: '#009DE0',
      },
      select: {
        background: 'B5B5C3',
        '&&:focus': {
          backgroundColor: '#fff',
        },
      },
    },

    MuiList: {
      padding: {
        paddingTop: 0,
        paddingBottom: 0,
      },
    },
    MuiFormControl: {
      marginNormal: {
        marginTop: 0,
      },
    },
    MuiPaper: {
      elevation1: {
        boxShadow: 'none',
      },
      rounded: {
        borderRadius: 5,
        boxShadow: 'none',
      },
    },
    MuiPaginationItem: {
      root: {
        color: '#B5B5C3',
      },

      sizeLarge: {
        width: 32,
        height: 32,
        minWidth: 'none',
      },
      page: {
        marginRight: 0,
        marginLeft: 6,
        '&$selected': {
          backgroundColor: '#009DE0',
          color: '#fff',
        },
      },
      outlined: {
        border: 'none',
      },
      rounded: {
        borderRadius: 5,
      },
    },
    MuiAutocomplete: {
      input: {
        padding: 2,
      },
      inputRoot: {
        padding: 0,
      },
    },
    MuiOutlinedInput: {
      notchedOutline: {
        borderColor: '#009DE0',
      },
    },
    MuiChip: {
      root: {
        backgroundColor: 'none',
      },
      deleteIcon: {
        display: 'none',
      },
    },
  },
})
