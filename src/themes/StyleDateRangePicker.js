import styled from 'styled-components'
import {DateRangePicker} from 'rsuite'
import 'rsuite/dist/styles/rsuite-default.css'

export const StyledDateRangePicker = styled(DateRangePicker)`
  border: 1px solid #009de0;
  border-radius: 5px;
  .rs-picker-toggle-caret {
    color: #009de0;
  }
`
