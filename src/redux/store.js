import {combineReducers} from 'redux'

import state from './widgets/stateWidget'

const rootReducer = combineReducers({
  state,
})

export default rootReducer
