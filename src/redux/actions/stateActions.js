export const SAVE_TO_STATE = 'actions/stateActions/SAVE_TO_STATE'
export const DELETE_INVOICE = 'actions/stateActions/DELETE_INVOICE'
export const EDIT_INVOICE = 'actions/stateActions/EDIT_INVOICE'

export const saveInvoice = invoice => {
  return {
    type: SAVE_TO_STATE,
    payload: invoice,
  }
}
export const deleteInvoice = invoice => {
  return {
    type: DELETE_INVOICE,
    payload: invoice,
  }
}
export const editInvoice = invoice => {
  return {
    type: EDIT_INVOICE,
    payload: invoice,
  }
}
