import {SAVE_TO_STATE, DELETE_INVOICE, EDIT_INVOICE} from '../actions/stateActions'

const initialState = {
  invoicesList: [],
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_TO_STATE:
      return {
        ...state,
        invoicesList: [...state.invoicesList, action.payload],
      }
    case DELETE_INVOICE:
      return {
        ...state,
        invoicesList: state.invoicesList.filter(x => x.id !== action.payload),
      }
    case EDIT_INVOICE: {
      return {
        ...state,
        invoicesList: state.invoicesList.map(x => {
          if (x.id === action.payload.id) {
            return action.payload
          }
          return x
        }),
      }
    }
    default:
      return state
  }
}

export default reducer
