export const dateData = [
  {label: 'Invoice data', name: 'invoiceDate'},
  {label: 'Due date', name: 'dueDate'},
]
