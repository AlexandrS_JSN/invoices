export const customerData = [
  {name: 'firstName', labelText: 'First Name*', placeholder: 'First Name'},
  {name: 'lastName', labelText: 'Last Name*', placeholder: 'Last Name'},
  {name: 'email', labelText: 'Email*', placeholder: 'example@gmail.com'},
  {name: 'address', labelText: 'Address', placeholder: 'Address'},
  {
    name: 'companyName',
    labelText: 'Company Name',
    placeholder: 'Company Name',
  },
  {name: 'phone', labelText: 'Phone', placeholder: '+1234567890'},
]
