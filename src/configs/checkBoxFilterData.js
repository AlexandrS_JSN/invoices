export const deliveryStatusConfig = [
  {name: 'deliverySent', label: 'Send'},
  {name: 'deliveryUnsent', label: 'Unsent'},
]
export const paymentStatusConfig = [
  {name: 'paymentPaid', label: 'Paid'},
  {name: 'paymentUnpaid', label: 'Unpaid'},
]
