export const configTableListHead = [
  {width: '8%', value: '#', align: 'left'},
  {width: '20%', value: 'REPORTS', align: 'left'},
  {width: '8%', value: 'ITEM', align: 'left'},
  {width: '12%', value: 'DATE SENT', align: 'left'},
  {width: '15%', value: 'DELIVERY STATUS', align: 'left'},
  {width: '15%', value: 'PAY STATUS', align: 'left'},
  {width: '6%', value: 'ACTION', align: 'left'},
]
