import {ThemeProvider} from '@material-ui/core'
import {DrawerTool} from './tools/DrawerTool'
import {appTheme} from './themes/appTheme'
import {MuiPickersUtilsProvider} from '@material-ui/pickers'
import DateFnsUtils from '@date-io/date-fns'

function App() {
  return (
    <ThemeProvider theme={appTheme}>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <DrawerTool />
      </MuiPickersUtilsProvider>
    </ThemeProvider>
  )
}

export default App
