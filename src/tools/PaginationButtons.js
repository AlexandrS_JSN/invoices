import React from 'react'
import Pagination from '@material-ui/lab/Pagination'
import {makeStyles} from '@material-ui/core'
import TablePagination from '@material-ui/core/TablePagination'

const useStyles = makeStyles(theme => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
    display: 'flex',
    justifyContent: 'space-between',
  },
  pagination: {
    '.MuiTablePagination-caption': {
      display: 'none',
    },
  },
}))

export function PaginationButtons(props) {
  const classes = useStyles()
  const {rowsPerPage, rows} = props

  return (
    <div className={classes.root}>
      <TablePagination
        className={classes.pagination}
        rowsPerPageOptions={[5, 10, 25, {label: 'All', value: -1}]}
        colSpan={3}
        count={rows.length}
        rowsPerPage={rowsPerPage}
        ActionsComponent={Pagination}
      />
    </div>
  )
}
