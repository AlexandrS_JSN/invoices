import React from 'react'
import {Box, Typography, Modal, Fade, Button, Backdrop, makeStyles} from '@material-ui/core'
import {useDispatch} from 'react-redux'
import {deleteInvoice} from '../redux/actions/stateActions'
import {IDelete} from '../images/icons/.'

const useStyles = makeStyles(theme => ({
  modal: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalPaper: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    border: 'none',
    borderRadius: 5,
    maxHeight: '90vh',
    minHeight: '20vh',
    padding: theme.spacing(2, 8, 2),
    width: 410,
    '& h2': {
      color: '#04406B',
    },
  },
  deleteImg: {
    cursor: 'pointer',
  },
}))

export function ModalTool({id}) {
  const classes = useStyles()
  const dispatch = useDispatch()

  const [modal, setModal] = React.useState(false)

  const handleOpenModal = () => {
    setModal(true)
  }

  const handleCloseModal = () => {
    setModal(false)
  }

  return (
    <>
      <img src={IDelete} alt='' onClick={handleOpenModal} className={classes.deleteImg} />
      <Modal
        className={classes.modal}
        open={modal}
        onClose={handleCloseModal}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 200,
        }}>
        <Fade in={modal}>
          <div className={classes.modalPaper}>
            <Box display='flex' justifyContent='center'>
              <Typography variant='h5'>Are you sure about that?</Typography>
            </Box>

            <Box display='flex' justifyContent='space-between'>
              <Button
                onClick={() => {
                  handleCloseModal()
                  dispatch(deleteInvoice(id))
                }}
                variant='outlined'
                color='primary'
                className={classes.modalButton}>
                Delete
              </Button>
              <Button onClick={handleCloseModal} variant='contained' color='primary' className={classes.modalButton}>
                Cancel
              </Button>
            </Box>
          </div>
        </Fade>
      </Modal>
    </>
  )
}
