import React from 'react'

import {
  AppBar,
  CssBaseline,
  Drawer,
  Hidden,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Toolbar,
  Typography,
  Box,
  Container,
} from '@material-ui/core'
import MenuIcon from '@material-ui/icons/Menu'
import {makeStyles} from '@material-ui/core/styles'
import {NavLink, Switch, Route} from 'react-router-dom'
import {Dashboard, Settings, Reports, Revenue, Support} from '../components'
import {IDashboard, IInvoices, IReports, ISettings, IRevenue, ISupport, IBell, IUser} from '../images/icons'
import Logo from '../images/Logo.png'
import {InvoicesList} from '../components/InvoicesList/InvoicesList'
import {Invoices} from '../components/Invoices/Invoices'

const drawerWidth = 332
const appBarHeight = 101

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    background: '#EEF4F8',
    minHeight: '100vh',
  },
  drawer: {
    [theme.breakpoints.up('lg')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up('lg')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
    boxShadow: 'none',
    color: '#04406B',
    background: '#fff',
    height: appBarHeight,
  },
  appBarText: {
    [theme.breakpoints.down('md')]: {
      fontSize: '1.2rem',
    },
    [theme.breakpoints.down('sm')]: {
      fontSize: '.5rem',
    },
    fontSize: 22,
    lineHeight: 30,
  },
  menuButton: {
    marginRight: theme.spacing(4),
    [theme.breakpoints.up('lg')]: {
      display: 'none',
    },
  },
  toolbar: theme.mixins.toolbar,
  toolbarStyle: {
    [theme.breakpoints.down('sm')]: {
      paddingLeft: theme.spacing(4),
    },
    paddingLeft: theme.spacing(9),
    height: appBarHeight,
  },
  drawerPaper: {
    width: drawerWidth,
    background: '#065188',
  },
  listLink: {
    display: 'block',
    textDecoration: 'none',
    color: '#B0CFE3',
    fontSize: 16,
    margin: '10px 0',
    textTransform: 'uppercase',
    width: '100%',
    '&:hover': {
      textDecoration: 'none',
      color: '#fff',
    },
    '&:focus': {
      textDecoration: 'none',
      color: '#fff',
    },
  },
  listItem: {
    padding: '14px 60px 14px 42px',
    borderRadius: 5,
  },
  activeClass: {
    background: 'rgba(1, 24, 41, .3)',
    borderRadius: 5,
    color: '#FBFBFB',
    transition: 'background .4s',
    filter: 'brightness(1.3)',
  },
  content: {
    flexGrow: 0,
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(4),
    paddingLeft: 0,
    background: '#E5E5E5',
    minHeight: '100vh',
  },
  wrapper: {
    [theme.breakpoints.down('md')]: {
      marginLeft: 0,
    },
    marginTop: theme.spacing(7),
    marginLeft: `calc(${drawerWidth}px + 72px)`,
  },
}))

export function DrawerTool(props) {
  const {window} = props
  const classes = useStyles()
  const [mobileOpen, setMobileOpen] = React.useState(false)

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen)
  }

  const menuItems = [
    {text: 'dashboard', icon: IDashboard},
    {text: 'invoices', icon: IInvoices},
    {text: 'revenue', icon: IRevenue},
    {text: 'reports', icon: IReports},
    {text: 'settings', icon: ISettings},
    {text: 'support', icon: ISupport},
  ]

  const drawer = (
    <Container>
      <Box display='flex' flexDirection='column'>
        <Box
          className={classes.logo}
          display='flex'
          justifyContent='center
        '>
          <img src={Logo} alt='' className={classes.logo} />
        </Box>
        <div className={classes.toolbar} />
        <Container>
          <List className={classes.list}>
            {menuItems.map(item => {
              const {text, icon} = item
              return (
                <NavLink
                  exact
                  to={`/${text}`}
                  key={text}
                  className={classes.listLink}
                  activeClassName={classes.activeClass}>
                  <ListItem button className={classes.listItem}>
                    <ListItemIcon>
                      <img className={classes.activeClass} src={icon} alt='' />
                    </ListItemIcon>
                    <ListItemText primary={text} />
                  </ListItem>
                </NavLink>
              )
            })}
          </List>
        </Container>
      </Box>
    </Container>
  )

  const container = window !== undefined ? () => window().document.body : undefined

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position='fixed' className={classes.appBar}>
        <Toolbar className={classes.toolbarStyle}>
          <IconButton
            color='inherit'
            aria-label='open drawer'
            edge='start'
            onClick={handleDrawerToggle}
            className={classes.menuButton}>
            <MenuIcon />
          </IconButton>
          <Container disableGutters={true} maxWidth='xl'>
            <Box display='flex' justifyContent='space-between'>
              <Container disableGutters={true} fixed>
                <Typography variant='h6'>Invoices</Typography>
              </Container>
              <Container disableGutters={true} fixed>
                <Box display='flex' justifyContent='flex-end' alignItems='center'>
                  <Box pr={4}>
                    <a href='##'>
                      <img src={IBell} alt='' />
                    </a>
                  </Box>
                  <Box pr={4}>
                    <a href='##'>
                      <img src={IUser} alt='' />
                    </a>
                  </Box>
                </Box>
              </Container>
            </Box>
          </Container>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label='mailbox folders'>
        <Hidden mdUp implementation='css'>
          <Drawer
            container={container}
            variant='temporary'
            anchor='left'
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true,
            }}>
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden mdDown implementation='css'>
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant='permanent'
            open>
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <div className={classes.wrapper}>
          <Switch>
            <Route path='/dashboard'>
              <Dashboard />
            </Route>
            <Route path='/invoices'>
              <InvoicesList />
            </Route>
            <Route path='/create'>
              <Invoices />
            </Route>
            <Route path={'/edit/:id'}>
              <Invoices />
            </Route>
            <Route path='/reports'>
              <Reports />
            </Route>
            <Route path='/revenue'>
              <Revenue />
            </Route>
            <Route path='/settings'>
              <Settings />
            </Route>
            <Route path='/support'>
              <Support />
            </Route>
          </Switch>
        </div>
      </main>
    </div>
  )
}
