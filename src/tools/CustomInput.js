import React from 'react'
import {FormControl, InputLabel} from '@material-ui/core'
import {StylesInput} from '../themes/StylesInput'

export function CustomInput({onChange, name, value}) {
  return (
    <FormControl>
      <InputLabel shrink>Recipient Email</InputLabel>
      <StylesInput placeholder='example@gmail.com' type='search' onChange={onChange} name={name} value={value} />
    </FormControl>
  )
}
