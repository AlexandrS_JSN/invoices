import React from 'react'
import {makeStyles, TableRow, TableCell, TextField, InputAdornment, IconButton} from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import ClearIcon from '@material-ui/icons/Clear'

const useStyles = makeStyles(theme => ({
  cardTitle: {
    padding: '15px 0 20px 0',
    fontSize: 20,
  },
  linkCard: {
    fontWeight: 600,
    fontSize: 16,
    paddingBottom: 20,
    cursor: 'pointer',
    lineHeight: '21.79px',
    color: '#000',
    '&:hover': {
      textDecoration: 'none',
    },
  },
  table: {
    border: '1px solid #009DE0',
    borderBottom: 'none',
    borderRadius: 5,
    boxShadow: 'none',
    fontWeight: 600,
  },
  tableTitle: {
    paddingRight: 50,
  },
  tableRow: {
    borderBottom: '1px solid #009DE0',
  },
  tableCell: {
    borderRight: '1px solid #009DE0',
    padding: 0,
  },
  tableCellPrimary: {
    color: '#009DE0',
    fontWeight: 600,
  },
  primaryTableRow: {
    background: '#EEF4F8',
    color: '#009DE0',
  },
  textField: {
    '& input[type=number]': {
      '-moz-appearance': 'textfield',
    },
    '& input[type=number]::-webkit-outer-spin-button': {
      '-webkit-appearance': 'none',
      margin: 0,
    },
    '& input[type=number]::-webkit-inner-spin-button': {
      '-webkit-appearance': 'none',
      margin: 0,
    },
  },
  textFieldMl: {
    marginLeft: 5,
  },
}))

export function CustomTableRow({number, name, onChange, index}) {
  const classes = useStyles()

  const [count, setCount] = React.useState('')
  const [price, setPrice] = React.useState('')
  const [total, setTotal] = React.useState('')

  function handleOnlyNumbers(e, method) {
    const onlyNumb = e.target.value.replace(/[^0-9]/g, '')
    method(onlyNumb)
    setTotal(count * price)
  }
  React.useEffect(() => {
    setTotal(count * price)
  }, [count, price])

  return (
    <TableRow>
      <TableCell className={`${classes.tableCell} ${classes.tableNumberCell}`} align='center' scope='row'>
        {number}
      </TableCell>
      <TableCell className={classes.tableCell}>
        <Autocomplete
          options={['hi']}
          value={name}
          className={classes.textField}
          disableClearable
          getOptionSelected={(option, value) => option.id === value.id}
          renderInput={params => (
            <TextField
              {...params}
              variant='outlined'
              onChange={event => onChange(event, index)}
              name='name'
              placeholder='Item name'
            />
          )}
        />
      </TableCell>
      <TableCell className={classes.tableCell}>
        <TextField
          placeholder='1'
          name='count'
          className={`${classes.textField} ${classes.textFieldMl}`}
          value={count}
          type='number'
          InputProps={{
            endAdornment: (
              <IconButton onClick={() => setCount('')}>
                <ClearIcon />
              </IconButton>
            ),
          }}
          onChange={e => {
            handleOnlyNumbers(e, setCount)
            onChange(e, index)
          }}
        />
      </TableCell>
      <TableCell className={classes.tableCell} align='left'>
        <TextField
          name='price'
          placeholder='350'
          value={price}
          InputProps={{
            startAdornment: <InputAdornment position='end'>{'$'}</InputAdornment>,
            endAdornment: (
              <IconButton onClick={() => setPrice('')}>
                <ClearIcon />
              </IconButton>
            ),
          }}
          onChange={e => {
            handleOnlyNumbers(e, setPrice)
            onChange(e, index)
          }}
        />
      </TableCell>
      <TableCell align='left' name='total'>
        {total}
      </TableCell>
    </TableRow>
  )
}
