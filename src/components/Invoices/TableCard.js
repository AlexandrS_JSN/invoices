import React from 'react'
import {
  Grid,
  Card,
  Typography,
  makeStyles,
  CardContent,
  Link,
  Paper,
  TableContainer,
  Table,
  TableCell,
  TableRow,
  TableHead,
  TableBody,
  Box,
  ThemeProvider,
} from '@material-ui/core'

import {CustomTableRow} from '../../tools/CustomTableRow'
import {inputTheme} from '../../themes/tableTheme'

const useStyles = makeStyles(theme => ({
  cardTitle: {
    padding: '15px 0 20px 0',
    fontSize: 20,
  },

  linkCard: {
    fontWeight: 600,
    fontSize: 16,
    width: 'fit-content',
    paddingBottom: 20,
    cursor: 'pointer',
    lineHeight: '21.79px',
    color: '#000',
    '&:hover': {
      textDecoration: 'none',
    },
  },
  table: {
    border: '1px solid #009DE0',
    borderBottom: 'none',
    borderRadius: 5,
    boxShadow: 'none',
    fontWeight: 600,
    width: '100%%',
    display: 'block',
    overflowX: 'visible',
  },
  tableContainer: {
    tableLayout: 'fixed',
  },
  tableTitle: {
    paddingRight: 50,
  },
  tableRow: {
    borderBottom: '1px solid #009DE0',
  },
  tableCell: {
    borderRight: '1px solid #009DE0',
  },
  tableCellPrimary: {
    color: '#009DE0',
    fontWeight: 600,
  },
  tableCellSecond: {
    color: '#B5B5C3',
  },
  tableHeight: {
    maxHeight: '30vh',
    overflow: 'auto',
  },
  primaryTableRow: {
    background: '#EEF4F8',
    color: '#009DE0',
  },
}))

export function TableCard({handleChangeTable, invoiceItems, handleAddInvoiceItem}) {
  const classes = useStyles()

  const tax = 100

  const totalCounter = () => {
    const total = invoiceItems.reduce((acc, curr) => acc + curr.price * curr.count, 0)
    return total
  }
  return (
    <Card>
      <CardContent>
        <Grid container display='flex' direction='column'>
          <Typography variant='h5' className={classes.cardTitle}>
            Invoice Items
          </Typography>
          <Grid item>
            <TableContainer component={Box} className={`${classes.table} ${classes.tableHeight}`}>
              <ThemeProvider theme={inputTheme}>
                <Table aria-label='caption table' className={classes.tableContainer}>
                  <TableHead className={classes.primaryTableRow}>
                    <TableRow className={classes.tableRow}>
                      <TableCell width='4%' className={`${classes.tableCell} ${classes.tableCellPrimary}`}>
                        #
                      </TableCell>
                      <TableCell
                        width='55%'
                        className={`${classes.tableCell} ${classes.tableCellPrimary}`}
                        align='left'>
                        ITEM
                      </TableCell>
                      <TableCell
                        width='10%'
                        className={`${classes.tableCell} ${classes.tableCellPrimary}`}
                        align='left'>
                        QTY
                      </TableCell>
                      <TableCell
                        width='14%'
                        className={`${classes.tableCell} ${classes.tableCellPrimary}`}
                        align='left'>
                        PRICE
                      </TableCell>
                      <TableCell className={classes.tableCellPrimary} width='17%' align='left'>
                        TOTAL
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {invoiceItems.map((cRow, index) => {
                      return (
                        <CustomTableRow key={cRow.number} {...cRow} onChange={handleChangeTable} index={index + 1} />
                      )
                    })}
                  </TableBody>
                </Table>
              </ThemeProvider>
            </TableContainer>
          </Grid>
          <Link className={classes.linkCard} onClick={handleAddInvoiceItem}>
            +Add invoice item
          </Link>
          <Grid item>
            <TableContainer component={Paper} className={classes.table}>
              <ThemeProvider theme={inputTheme}>
                <Table aria-label='caption table'>
                  <TableBody>
                    <TableRow>
                      <TableCell width='69%' />
                      <TableCell width='14%' className={classes.tableCellSecond}>
                        SUBTOTAL:
                      </TableCell>
                      <TableCell width='17%'>{`$${totalCounter()} USD`}</TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell />
                      <TableCell className={classes.tableCellSecond}>TAX:</TableCell>
                      <TableCell>{`$${tax} USD`}</TableCell>
                    </TableRow>
                    <TableRow className={classes.primaryTableRow}>
                      <TableCell />
                      <TableCell align='left' className={classes.tableCellPrimary}>
                        TOTAL:
                      </TableCell>
                      <TableCell className={classes.tableCellPrimary}>
                        {`$${totalCounter() > tax ? totalCounter() - tax : 0} USD`}
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </ThemeProvider>
            </TableContainer>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  )
}
