import React from 'react'
import {Grid, Button, Container, makeStyles} from '@material-ui/core'

import {CustomersCard, DetailsCard, ReportsCard, TableCard, ButtonsContainer} from '.'
import {useDispatch} from 'react-redux'
import {saveInvoice, editInvoice} from '../../redux/actions/stateActions'
import {useLocation} from 'react-router'

const useStyles = makeStyles(theme => ({
  container: {
    margin: 0,
    maxWidth: '100%',
  },
  gridItem: {
    display: 'flex',
    alignItems: 'stretch',
    width: '100%',
  },
  button: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
}))

export function Invoices() {
  const classes = useStyles()
  const dispatch = useDispatch()
  const location = useLocation()
  const initialState = location.state || {
    id: Math.floor(Math.random() * 541),
    firstName: '',
    lastName: '',
    email: '',
    address: '',
    companyName: '',
    phone: '',
    addToContact: false,
    recipients: [],
    invoiceDate: '3/22/2021',
    dueDate: '3/22/2021',
    invoiceNo: '',
    currency: 'USD',
    reports: false,
    invoiceItems: [
      {
        number: 1,
        name: '',
        count: '',
        price: '',
      },
    ],
    paymentStatus: false,
    deliveryStatus: null,
  }
  const [state, setState] = React.useState(initialState)

  const handleChangeCustomers = event => {
    setState(prev => ({...prev, [event.target.name]: event.target.value}))
  }
  const handleAddToContacts = () => {
    setState(prev => ({
      ...prev,
      addToContact: !prev.addToContact,
    }))
  }
  const setRecipients = recipientsState => {
    setState({...state, recipients: recipientsState.map(x => x.value)})
  }

  const handleDate = (event, name) => {
    setState(prev => ({
      ...prev,
      [name]: event.toLocaleDateString('en-US'),
    }))
  }
  const handleChangeDate = event => {
    setState(prev => ({...prev, [event.target.name]: event.target.value}))
  }

  const handleReports = () => {
    setState(prev => ({
      ...prev,
      reports: !prev.reports,
    }))
  }

  const handleChangeTable = (event, index) => {
    const newElements = state.invoiceItems.map(dataItem => {
      if (index === dataItem.number && dataItem.hasOwnProperty(event.target.name)) {
        return {
          ...dataItem,
          [event.target.name]: event.target.value,
        }
      }
      return dataItem
    })
    setState(prev => ({
      ...prev,
      invoiceItems: newElements,
    }))
  }
  const handleAddInvoiceItem = () => {
    setState(prev => ({
      ...prev,
      invoiceItems: [...prev.invoiceItems, {number: prev.invoiceItems.length + 1, name: '', count: '', price: ''}],
    }))
  }
  const handlePaymentStatus = () => {
    setState(prev => ({...prev, paymentStatus: !prev.paymentStatus}))
  }

  const handleSendDeliveryStatus = () => {
    setState(prev => ({...prev, deliveryStatus: true}))
  }
  const handleSendAsDraftDeliveryStatus = () => {
    setState(prev => ({...prev, deliveryStatus: false}))
  }

  React.useEffect(() => {
    if (location.state) {
      dispatch(editInvoice(state))
    } else if (state.deliveryStatus) {
      dispatch(saveInvoice(state))
    }
  }, [state.deliveryStatus, location.state, dispatch, state])

  return (
    <Container className={classes.container}>
      <Grid container direction='row' spacing={3} display='flex' alignItems='stretch'>
        <Grid item md={4} />
        <Grid item md={4} className={classes.button}>
          <Button variant='contained' color='primary'>
            Use Invoice Template
          </Button>
        </Grid>
        <Grid item md={3} />
        <Grid item md={4} className={classes.gridItem}>
          <CustomersCard
            value={[state.firstName, state.lastName, state.email, state.address, state.companyName, state.phone]}
            addToContact={state.addToContact}
            recipients={state.recipients}
            onAddContact={handleAddToContacts}
            handleChange={handleChangeCustomers}
            handleRecipients={setRecipients}
          />
        </Grid>
        <Grid item md={4} className={classes.gridItem}>
          <DetailsCard
            handleDate={handleDate}
            handleChangeDate={handleChangeDate}
            currency={state.currency}
            invoiceNo={state.invoiceNo}
            invoiceDate={state.invoiceDate}
            dueDate={state.dueDate}
          />
        </Grid>
        <Grid item md={4} lg={3} className={classes.gridItem}>
          <ReportsCard handleReports={handleReports} reports={state.reports} />
        </Grid>
        <Grid item lg={8} md={12} className={classes.gridItem}>
          <TableCard
            handleChangeTable={handleChangeTable}
            invoiceItems={state.invoiceItems}
            handleAddInvoiceItem={handleAddInvoiceItem}
          />
        </Grid>
        <Grid item lg={12} md={12} className={classes.gridItem}>
          <ButtonsContainer
            onSendData={handleSendDeliveryStatus}
            paymentStatus={state.paymentStatus}
            handlePaymentStatus={handlePaymentStatus}
            saveAsDraft={handleSendAsDraftDeliveryStatus}
          />
        </Grid>
      </Grid>
    </Container>
  )
}
