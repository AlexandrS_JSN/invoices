/* eslint-disable react/jsx-no-duplicate-props */
import React from 'react'
import {
  Grid,
  Card,
  Typography,
  makeStyles,
  CardContent,
  InputLabel,
  FormControl,
  Select,
  MenuItem,
} from '@material-ui/core'

import {StylesInput} from '../../themes/StylesInput'
import {IUsa, ICanada, IDate} from '../../images/icons'
import {KeyboardDatePicker} from '@material-ui/pickers'
import {dateData} from '../../configs/dateData'

const useStyles = makeStyles(theme => ({
  cardTitle: {
    padding: '15px 0 20px 0',
    fontSize: 20,
  },
  inputLabel: {
    color: '#000',
    fontWeight: 600,
  },
  linkCard: {
    fontWeight: 600,
    fontSize: 16,
    paddingBottom: 20,
    cursor: 'pointer',
    lineHeight: '21.79px',
    color: '#000',
    '&:hover': {
      textDecoration: 'none',
    },
  },
  menuItem: {
    display: 'flex',
    alignItems: 'center',
  },
  menuItemImg: {
    marginRight: 10,
  },
}))

export function DetailsCard({handleDate, handleChangeDate, currency, invoiceNo, invoiceDate, dueDate}) {
  const classes = useStyles()
  const dateObj = {invoiceDate, dueDate}

  return (
    <Card>
      <CardContent>
        <Typography gutterBottom className={classes.cardTitle}>
          Invoice Details
        </Typography>
        <form>
          <Grid container spacing={6}>
            {dateData.map((date, index) => {
              const {name, label} = date
              return (
                <Grid item md={6} sm={6} xs={12} key={date.name}>
                  <FormControl>
                    <KeyboardDatePicker
                      disableToolbar
                      variant='inline'
                      format='M/dd/yyyy'
                      margin='normal'
                      name={name}
                      value={Object.values(dateObj)[index]}
                      label={label}
                      multiple
                      onChange={event => handleDate(event, name)}
                      KeyboardButtonProps={{
                        'aria-label': 'change date',
                      }}
                      keyboardIcon={<img src={IDate} alt='calendar' width='24px' height='24px' />}
                    />
                  </FormControl>
                </Grid>
              )
            })}
            <Grid item md={6} sm={6} xs={12}>
              <FormControl>
                <InputLabel className={classes.inputLabel}>Invoice no</InputLabel>
                <StylesInput name='invoiceNo' onChange={handleChangeDate} value={invoiceNo} />
              </FormControl>
            </Grid>
            <Grid item md={6} lg={6} sm={6} xs={12}>
              <FormControl>
                <InputLabel id='currency-select'>Currency</InputLabel>
                <Select
                  labelId='currency-select'
                  autoWidth
                  name='currency'
                  displayEmpty
                  value={currency}
                  onChange={handleChangeDate}>
                  <MenuItem value='USD' name='USD' className={classes.menuItem}>
                    <img src={IUsa} width='28px' height='17px' alt='' className={classes.menuItemImg} />
                    <span>USA - US dollar</span>
                  </MenuItem>
                  <MenuItem value='CAD' name='CAD' className={classes.menuItem}>
                    <img src={ICanada} width='28px' height='17px' alt='' className={classes.menuItemImg} />
                    <span>CAD - Canadian dollar</span>
                  </MenuItem>
                </Select>
              </FormControl>
            </Grid>
          </Grid>
        </form>
      </CardContent>
    </Card>
  )
}
