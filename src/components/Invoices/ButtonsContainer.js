import React from 'react'
import {Grid, Button, makeStyles, InputLabel, FormControl, Box, Fade, Backdrop, Modal} from '@material-ui/core'

import {StylesInput} from '../../themes/StylesInput'
import {useHistory} from 'react-router-dom'

const useStyles = makeStyles(theme => ({
  buttonsL: {
    marginRight: 24,
    '&:last-child': {
      marginRight: 0,
    },
    width: 'fit-content',
  },
  buttonsR: {
    marginLeft: 24,
    '&:first-child': {
      marginLeft: 0,
    },
    width: 'fit-content',
  },
  buttonsBoxL: {
    display: 'flex',
    justifyContent: 'flex-end',
    [theme.breakpoints.down('md')]: {
      justifyContent: 'center',
    },
  },
  buttonsBoxR: {
    display: 'flex',
    justifyContent: 'flex-start',
    [theme.breakpoints.down('md')]: {
      justifyContent: 'center',
    },
  },
  modal: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalPaper: {
    backgroundColor: '#fff',
    border: 'none',
    borderRadius: 5,
    padding: theme.spacing(2, 8, 2),
    width: 410,
    '& h2': {
      color: '#04406B',
    },
  },
  modalButton: {
    margin: '50px 0 20px 0',
    padding: '9px 50px',
  },
}))

export function ButtonsContainer({onSendData, handlePaymentStatus, paymentStatus, saveAsDraft}) {
  const classes = useStyles()
  const history = useHistory()

  const [openModalTemplate, setOpenModalTemplate] = React.useState(false)

  const handleOpenModalTemplate = () => {
    setOpenModalTemplate(true)
  }

  const handleCloseModalTemplate = () => {
    setOpenModalTemplate(false)
  }
  return (
    <Grid
      container
      spacing={5}
      display='flex'
      flex-direction='grow'
      justify-content='space-between'
      className={classes.gridContainer}>
      <Grid item lg={4} md={6} sm={12} xs={12}>
        <Box className={`${classes.buttonsBox} ${classes.buttonsBoxR}`}>
          <Button color='primary' className={classes.buttonsL}>
            Delete
          </Button>
          <Button color='primary' className={classes.buttonsL}>
            Cancel
          </Button>
          <Button color='primary' className={classes.buttonsL} onClick={handlePaymentStatus}>
            Mark as {paymentStatus ? 'Unpaid' : 'Paid'}
          </Button>
        </Box>
      </Grid>
      <Grid item lg={4} md={6} sm={12} xs={12}>
        <Box className={`${classes.buttonsBox} ${classes.buttonsBoxL}`}>
          <>
            <Button onClick={handleOpenModalTemplate} variant='contained' color='primary' className={classes.buttonsR}>
              Save as Template
            </Button>
            <Modal
              className={classes.modal}
              open={openModalTemplate}
              onClose={handleCloseModalTemplate}
              closeAfterTransition
              BackdropComponent={Backdrop}
              BackdropProps={{
                timeout: 200,
              }}>
              <Fade in={openModalTemplate}>
                <div className={classes.modalPaper}>
                  <h2>Please enter a Template Name</h2>
                  <FormControl>
                    <InputLabel shrink className={classes.inputLabel}>
                      Template Name
                    </InputLabel>
                    <StylesInput placeholder='Template Name' />
                  </FormControl>
                  <Box display='flex' justifyContent='space-between'>
                    <Button
                      onClick={handleCloseModalTemplate}
                      variant='outlined'
                      color='primary'
                      className={classes.modalButton}>
                      Cancel
                    </Button>
                    <Button
                      onClick={handleCloseModalTemplate}
                      variant='contained'
                      color='primary'
                      className={classes.modalButton}>
                      Save
                    </Button>
                  </Box>
                </div>
              </Fade>
            </Modal>
          </>

          <Button
            variant='contained'
            color='primary'
            className={classes.buttonsR}
            onClick={() => {
              saveAsDraft()
              history.goBack()
            }}>
            Save as Draft
          </Button>
          <Button
            variant='contained'
            color='primary'
            className={classes.buttonsR}
            onClick={() => {
              onSendData()
              history.goBack()
            }}>
            Send
          </Button>
        </Box>
      </Grid>
    </Grid>
  )
}
