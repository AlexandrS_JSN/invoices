import React from 'react'
import {Card, CardContent, Grid, Typography, Link, FormControlLabel, Checkbox, makeStyles} from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  cardTitle: {
    padding: '15px 0 20px 0',
    fontSize: 20,
  },
  linkCard: {
    fontWeight: 600,
    fontSize: 16,
    paddingBottom: 20,
    cursor: 'pointer',
    lineHeight: '21.79px',
    color: '#000',
    '&:hover': {
      textDecoration: 'none',
    },
  },
}))

export function ReportsCard({handleReports, reports}) {
  const classes = useStyles()

  return (
    <Card>
      <CardContent>
        <Grid container display='flex' direction='column'>
          <Typography variant='h5' className={classes.cardTitle}>
            Report(s):
          </Typography>
          <Link className={classes.linkCard}>+Add report</Link>
          <FormControlLabel
            control={<Checkbox onChange={handleReports} color='primary' checked={reports} />}
            label='Require payment of invoice before sending report (s)'
          />
        </Grid>
      </CardContent>
    </Card>
  )
}
