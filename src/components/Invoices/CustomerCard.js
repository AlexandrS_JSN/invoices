import React from 'react'
import {
  Card,
  CardContent,
  Typography,
  Grid,
  FormControl,
  FormControlLabel,
  InputLabel,
  Checkbox,
  Link,
  Modal,
  Button,
  Box,
  makeStyles,
  Backdrop,
  Fade,
} from '@material-ui/core'
import {IUser} from '../../images/icons/.'
import {StylesInput} from '../../themes/StylesInput'

import {CustomInput} from '../../tools/CustomInput'
import {customerData} from '../../configs/customerData'

const useStyles = makeStyles(theme => ({
  cardTitle: {
    padding: '15px 0 20px 0',
    fontSize: 20,
  },
  linkCard: {
    fontWeight: 600,
    fontSize: 16,
    paddingBottom: 20,
    cursor: 'pointer',
    lineHeight: '21.79px',
    color: '#000',
    '&:hover': {
      textDecoration: 'none',
    },
  },
  recipientsButton: {
    position: 'relative',
  },
  recipientsImg: {
    position: 'absolute',
    right: -33,
  },
  recipientsImg2: {
    right: -45,
  },
  modal: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalPaper: {
    backgroundColor: '#fff',
    border: 'none',
    borderRadius: 5,
    maxHeight: '90vh',
    overflow: 'auto',
    padding: theme.spacing(2, 8, 2),
    width: 410,
    '& h2': {
      color: '#04406B',
    },
  },
  modalButton: {
    margin: '50px 0 20px 0',
    padding: '9px 50px',
  },
  recipientsInput: {
    marginTop: 15,
  },
  linkCardRecipients: {
    marginTop: 20,
    display: 'block',
    width: 'fit-content',
  },
}))

export function CustomersCard({addToContact, recipients, onAddContact, handleChange, handleRecipients, value}) {
  const classes = useStyles()
  const [openModalRecipients, setOpenModalRecipients] = React.useState(false)
  const [recipientsState, setRecipientsState] = React.useState([{name: String(Date.now()), value: ''}])

  const handleOpenModal = () => {
    setOpenModalRecipients(true)
  }
  const handleCloseModal = () => {
    setOpenModalRecipients(false)
  }

  const handleAddInput = () => {
    setRecipientsState(prev => [...prev, {name: String(Date.now()), value: ''}])
  }
  const handleClearRecipients = () => {
    setRecipientsState([{name: String(Date.now()), value: ''}])
  }
  const onChangeHandler = event => {
    const elementToChange = recipientsState.map(item => {
      if (item.name === event.target.name) {
        return {
          name: item.name,
          value: event.target.value,
        }
      }
      return item
    })
    setRecipientsState(elementToChange)
  }

  const recipientsImgs = (
    <>
      <img className={classes.recipientsImg} src={IUser} alt='' width={20} height={20} />
      <img className={`${classes.recipientsImg} ${classes.recipientsImg2}`} src={IUser} alt='' width={20} height={20} />
    </>
  )

  return (
    <Card>
      <CardContent>
        <Typography gutterBottom className={classes.cardTitle}>
          Customer Details
        </Typography>
        <Grid container spacing={6}>
          {customerData.map((item, index) => {
            const {name, labelText, placeholder} = item
            return (
              <Grid item md={6} sm={4} xs={6} key={name}>
                <FormControl>
                  <InputLabel shrink>{labelText}</InputLabel>
                  <StylesInput placeholder={placeholder} name={name} value={value[index]} onChange={handleChange} />
                </FormControl>
              </Grid>
            )
          })}
        </Grid>
        <Grid container display='flex' alignItems='center' justifyContent='flex-start' spacing={4}>
          <Grid item sm={6} md={12} lg={6}>
            <FormControlLabel
              control={<Checkbox onChange={onAddContact} name='addCustomer' color='primary' checked={addToContact} />}
              label='Add customer to contacts'
            />
          </Grid>
          <Grid item sm={6} md={12} lg={6}>
            <>
              <Link onClick={handleOpenModal} className={`${classes.linkCard} ${classes.recipientsButton}`}>
                + Add recipients {recipients.length > 0 && recipientsImgs}
              </Link>
              <Modal
                className={classes.modal}
                open={openModalRecipients}
                onClose={handleCloseModal}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                  timeout: 200,
                }}>
                <Fade in={openModalRecipients}>
                  <Box className={classes.modalPaper}>
                    <h2>Add recipients</h2>
                    {recipientsState.map(cInput => (
                      <CustomInput {...cInput} onChange={onChangeHandler} key={cInput.name} />
                    ))}
                    <Link className={`${classes.linkCard} ${classes.linkCardRecipients}`} onClick={handleAddInput}>
                      + Add One More Recipient
                    </Link>
                    <Box display='flex' justifyContent='space-between'>
                      <Button
                        variant='outlined'
                        color='primary'
                        className={classes.modalButton}
                        onClick={() => {
                          handleCloseModal()
                          handleClearRecipients()
                        }}>
                        Cancel
                      </Button>
                      <Button
                        onClick={() => {
                          handleCloseModal()
                          handleRecipients(recipientsState)
                        }}
                        variant='contained'
                        color='primary'
                        className={classes.modalButton}>
                        Save
                      </Button>
                    </Box>
                  </Box>
                </Fade>
              </Modal>
            </>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  )
}
