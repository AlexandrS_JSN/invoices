import React from 'react'

import {IFilter} from '../../images/icons/.'
import SearchIcon from '@material-ui/icons/Search'
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown'
import {
  Grid,
  Button,
  makeStyles,
  InputAdornment,
  Card,
  CardContent,
  Box,
  FormControlLabel,
  Checkbox,
  Typography,
  TextField,
} from '@material-ui/core'
import {StylesSearchInput} from '../../themes/StylesSearchInput'
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank'
import CheckBoxIcon from '@material-ui/icons/CheckBox'
import {deliveryStatusConfig, paymentStatusConfig} from '../../configs/checkBoxFilterData'
import {StyledDateRangePicker} from '../../themes/StyleDateRangePicker'
import Autocomplete from '@material-ui/lab/Autocomplete'
import {useHistory} from 'react-router-dom'

const icon = <CheckBoxOutlineBlankIcon fontSize='small' />
const checkedIcon = <CheckBoxIcon fontSize='small' color='primary' />

const useStyles = makeStyles(theme => ({
  filterBtn: {
    background: '#fff',
    fontSize: 18,
    display: 'flex',
    marginRight: '10px',
    justifyContent: 'center',
    '& span': {
      padding: '0 10px',
    },
  },
  filterBtnContainer: {
    order: 0,
  },
  searchInput: {
    width: '100%',
    order: 1,
  },
  showFilter: {
    display: 'block',
  },
  hideFilter: {
    display: 'none',
  },
  inputSearch: {
    border: 'none',
    outline: 'none',
    background: '#fff',
    width: '100%',
    fontSize: 18,
    height: 'auto',
    '.MuiInputBase': {
      input: {
        height: 'none',
      },
    },
  },
  filterPanel: {
    padding: '20px 50px 20px 40px',
  },
  checkBoxFilter: {
    padding: '5px 9px',
  },
  invoiceBtn: {
    display: 'flex',
    justifyContent: 'flex-end',
    order: 2,
    [theme.breakpoints.down('md')]: {
      order: 0,
    },
  },
  filterBoxBtn: {
    padding: '9px 27px',
  },

  titleFilter: {
    fontSize: 14,
    color: '#2D384E',
    fontWeight: 600,
    paddingBottom: 10,
  },
}))

export function Filter({onSendFilter, onSendSearchFilter}) {
  const classes = useStyles()
  let history = useHistory()

  function handleClick() {
    history.push('/create')
  }

  const [showFilter, setShowFilter] = React.useState(false)
  const initialFilterState = {
    deliveryStatus: {deliverySent: false, deliveryUnsent: false},
    paymentStatus: {
      paymentPaid: false,
      paymentUnpaid: false,
    },
    item: [],
    invoiceDate: '',
  }
  const [filterData, setFilterData] = React.useState(initialFilterState)

  const handleCheckValue = e => {
    const delivery = /delivery/i
    if (delivery.test(e.target.name)) {
      setFilterData(prev => ({
        ...prev,
        deliveryStatus: {...prev.deliveryStatus, [e.target.name]: !prev.deliveryStatus[e.target.name]},
      }))
    } else {
      setFilterData(prev => ({
        ...prev,
        paymentStatus: {...prev.paymentStatus, [e.target.name]: !prev.paymentStatus[e.target.name]},
      }))
    }
  }

  const handleAutocomplete = (e, value) => {
    setFilterData(prev => ({...prev, item: value}))
  }
  const handleDatePick = e => {
    setFilterData(prev => ({...prev, invoiceDate: e.map(x => x.toLocaleDateString('en-US'))}))
  }

  const handleClearFilter = () => {
    setFilterData(initialFilterState)
  }

  const deliveryStatusValues = Object.values(filterData.deliveryStatus)
  const paymentStatusValues = Object.values(filterData.paymentStatus)

  return (
    <Grid container spacing={4}>
      <Grid item xl={12} lg={12} md={12} sm={12} xs={12}>
        <Grid container display='flex' alignItems='center' direction='row' justifyContent='space-between' spacing={4}>
          <Grid item lg={2} md={6} xs={6} sm={6} className={classes.filterBtnContainer}>
            <Button color='primary' className={classes.filterBtn} onClick={() => setShowFilter(!showFilter)}>
              <img src={IFilter} alt='' />
              <span>Filter</span>
              <ArrowDropDownIcon />
            </Button>
          </Grid>
          <Grid item lg={8} md={12} xs={12} sm={12} className={classes.searchInput}>
            <StylesSearchInput
              placeholder='Search recipient, reports, invoice number, item'
              variant='standard'
              onChange={onSendSearchFilter}
              startAdornment={
                <InputAdornment position='end' className={classes.inputSearchImg}>
                  <SearchIcon />
                </InputAdornment>
              }
            />
          </Grid>
          <Grid item lg={2} md={6} xs={6} sm={6} className={classes.invoiceBtn}>
            <Button color='primary' variant='contained' type='button' onClick={handleClick}>
              + New Invoice
            </Button>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xl={10} lg={10} md={12} xs={12} className={showFilter ? classes.showFilter : classes.hideFilter}>
        <Grid container display='flex' alignItems='center' direction='row' justifyContent='space-between'>
          <Grid item lg={12} md={12} xs={12} xl={12}>
            <Card>
              <CardContent>
                <Grid container spacing={4}>
                  <Grid item lg={2} md={2} xs={6} sm={2}>
                    <Box display='flex' flexDirection='column'>
                      <Typography body1='span' component='div' className={classes.titleFilter}>
                        Delivery status
                      </Typography>
                      {deliveryStatusConfig.map((item, index) => (
                        <FormControlLabel
                          key={item.name}
                          className={classes.formBoxFilter}
                          control={
                            <Checkbox
                              checked={deliveryStatusValues[index]}
                              disableRipple
                              onChange={handleCheckValue}
                              name={item.name}
                              color='primary'
                              className={classes.checkBoxFilter}
                            />
                          }
                          label={item.label}
                        />
                      ))}
                    </Box>
                  </Grid>
                  <Grid item lg={2} md={2} xs={6} sm={2}>
                    <Box display='flex' flexDirection='column'>
                      <Typography body1='span' component='div' className={classes.titleFilter}>
                        Payment status
                      </Typography>
                      {paymentStatusConfig.map((item, index) => (
                        <FormControlLabel
                          key={item.name}
                          className={classes.formBoxFilter}
                          control={
                            <Checkbox
                              checked={paymentStatusValues[index]}
                              disableRipple
                              onChange={handleCheckValue}
                              name={item.name}
                              color='primary'
                              className={classes.checkBoxFilter}
                            />
                          }
                          label={item.label}
                        />
                      ))}
                    </Box>
                  </Grid>
                  <Grid item lg={4} md={4} xs={12} sm={4}>
                    <Box display='flex' flexDirection='column'>
                      <Typography body1='span' component='div' className={classes.titleFilter}>
                        Item
                      </Typography>
                      <Autocomplete
                        multiple
                        value={filterData.item}
                        options={[]}
                        disableCloseOnSelect
                        onChange={handleAutocomplete}
                        getOptionLabel={option => option}
                        renderOption={(option, {selected}) => (
                          <>
                            <Checkbox icon={icon} checkedIcon={checkedIcon} checked={selected} />
                            {option}
                          </>
                        )}
                        renderInput={params => (
                          <TextField {...params} size='small' variant='outlined' label='Choose an item' />
                        )}
                      />
                    </Box>
                  </Grid>
                  <Grid item lg={4} md={4} xs={12} sm={4}>
                    <Box display='flex' flexDirection='column'>
                      <Typography body1='span' component='div' className={classes.titleFilter} noWrap={false}>
                        Invoice creation date
                      </Typography>
                      <StyledDateRangePicker showOneCalendar onChange={handleDatePick} />
                    </Box>
                  </Grid>
                </Grid>
                <Grid container justifyContent='flex-end' spacing={4}>
                  <Grid item xl={4} lg={4} md={4} xs={12} sm={4}>
                    <Box display='flex' justifyContent='space-between'>
                      <Button
                        variant='contained'
                        color='primary'
                        className={classes.filterBoxBtn}
                        onClick={() => {
                          setShowFilter(!showFilter)
                          onSendFilter(filterData)
                        }}>
                        Apply
                      </Button>
                      <Button
                        variant='outlined'
                        color='primary'
                        className={classes.filterBoxBtn}
                        onClick={() => {
                          setShowFilter(!showFilter)
                          handleClearFilter()
                        }}>
                        Cancel
                      </Button>
                    </Box>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}
