import React from 'react'

import {Grid, makeStyles, Container, ThemeProvider} from '@material-ui/core'
import {Filter, List} from '.'
import {listTheme} from '../../themes/listTheme'
import {useSelector} from 'react-redux'

const useStyles = makeStyles(theme => ({
  container: {
    margin: 0,
    maxWidth: '100%',
  },
}))

export function InvoicesList() {
  const classes = useStyles()

  const [searchFilter, setSearchFilter] = React.useState(null)
  const [filtering, setFiltering] = React.useState(null)
  const state = useSelector(state =>
    state.state.invoicesList.filter(invoice => {
      if (searchFilter) {
        const regex = new RegExp(searchFilter)
        if (regex.test(invoice.invoiceNo) || regex.test([...invoice.recipients])) {
          return true
        }
      }

      if (!filtering || !searchFilter) {
        return true
      }

      const {deliverySent, deliveryUnsent} = filtering.deliveryStatus
      const {paymentPaid, paymentUnpaid} = filtering.paymentStatus
      const dateParsing =
        Date.parse(invoice.invoiceDate) > Date.parse(filtering.invoiceDate[0]) &&
        Date.parse(invoice.invoiceDate) < Date.parse(filtering.invoiceDate[1])
      if (
        dateParsing ||
        (deliverySent && invoice.deliveryStatus) ||
        (deliveryUnsent && !invoice.deliveryStatus) ||
        (paymentPaid && invoice.paymentStatus) ||
        (paymentUnpaid && !invoice.paymentStatus)
      ) {
        return true
      }

      return false
    }),
  )

  const handleSearchFilter = e => {
    setSearchFilter(e.target.value)
  }

  const handleSorting = filteringState => {
    setFiltering(filteringState)
  }
  return (
    <ThemeProvider theme={listTheme}>
      <Container className={classes.container}>
        <Grid container direction='row' spacing={3} display='flex' alignItems='center'>
          <Grid item xl={11} lg={11} md={12} xs={12}>
            <Filter onSendFilter={handleSorting} onSendSearchFilter={handleSearchFilter} />
          </Grid>
          <Grid item xl={11} lg={11} md={12} xs={12}>
            <List invoicesList={state} />
          </Grid>
        </Grid>
      </Container>
    </ThemeProvider>
  )
}
