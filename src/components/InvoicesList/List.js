import React from 'react'

import {makeStyles} from '@material-ui/core/styles'

import {
  Paper,
  Box,
  Card,
  FormControl,
  CardContent,
  Select,
  MenuItem,
  Typography,
  TableHead,
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableContainer,
} from '@material-ui/core'
import {configTableListHead} from '../../configs/configTableListHead'
import {Pagination} from '@material-ui/lab'
import {StylesSelectorInput} from '../../themes/StylesSelectorInput'
import {IEdit} from '../../images/icons/.'

import usePagination from '../../hooks/usePagination'

import {Link} from 'react-router-dom'

import {ModalTool} from '../../tools/ModalTool'

const useStyles = makeStyles(theme => ({
  table: {
    minWidth: 500,
  },
  cardTitle: {
    padding: '20px 0 20px 0',
    fontSize: 20,
  },
  resultPage: {
    display: 'flex',
    alignItems: 'center',
    fontSize: 14,
    marginLeft: 18,
    '& span': {
      color: '#B5B5C3',
      '& span': {
        marginLeft: 5,
        color: '#009DE0',
      },
    },
  },
  paginationBtn: {
    marginTop: 24,
  },
  tableHeadRow: {
    '& th': {
      fontSize: 16,
      color: '#B5B5C3',
    },
  },
  actionCell: {
    display: 'flex',
    justifyContent: 'space-evenly',
  },
  noneContainer: {
    background: 'rgba(3, 210, 255, .3)',
    borderRadius: 5,
    display: 'inline',
    '& span': {
      padding: '2px 7px',
      fontSize: 14,
      color: '#009DE0',
    },
  },
  confirm: {
    background: 'rgba(201, 247, 245, 1)',
    borderRadius: 5,
    display: 'inline',
    '& span': {
      padding: '2px 7px',
      fontSize: 14,
      color: '#1BC5CA',
    },
  },
  unconfirmed: {
    background: 'rgba(255, 226, 229, 1)',
    borderRadius: 5,
    display: 'inline',
    '& span': {
      padding: '2px 7px',
      fontSize: 14,
      color: '#F65A6D',
    },
  },
}))

export function List({invoicesList}) {
  const classes = useStyles()
  const DEFAULT_ELEMENTS_PER_PAGE = 5
  const [page, setPage] = React.useState(1)
  const [perPage, setPerPage] = React.useState(DEFAULT_ELEMENTS_PER_PAGE)

  const count = Math.ceil(invoicesList.length / perPage)
  const paginationData = usePagination(invoicesList, perPage)

  const handleChange = (event, paginationPage) => {
    setPage(paginationPage)
    paginationData.jump(paginationPage)
  }
  const handleChangePerPage = event => {
    setPerPage(event.target.value)
  }
  const resultPage = () => {
    const showFrom = (page - 1) * perPage + 1
    const showTo = showFrom + perPage
    const result = showTo > invoicesList.length ? invoicesList.length + 1 : showTo
    return `${showFrom}-${result - 1} of ${invoicesList.length}`
  }

  const none = (
    <div className={classes.noneContainer}>
      <span className={classes.none}>None</span>
    </div>
  )

  const statusMessage = text => (
    <div className={text === 'Sent' || text === 'Paid' ? classes.confirm : classes.unconfirmed}>
      <span>{text}</span>
    </div>
  )

  return (
    <>
      <Card>
        <CardContent>
          <Typography gutterBottom className={classes.cardTitle}>
            Customer Details
          </Typography>
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label='custom pagination table'>
              <TableHead>
                <TableRow className={classes.tableHeadRow}>
                  {configTableListHead.map(item => {
                    const {width, value, align} = item
                    return (
                      <TableCell align={align} width={width} key={value}>
                        {value}
                      </TableCell>
                    )
                  })}
                </TableRow>
              </TableHead>
              <TableBody>
                {paginationData.currentData().map((row, index) => (
                  <TableRow key={row.id}>
                    <TableCell align='left'>{index + 1}</TableCell>
                    <TableCell align='left'>{none}</TableCell>
                    <TableCell align='left'>Item</TableCell>
                    <TableCell align='left'>{row.invoiceDate}</TableCell>
                    <TableCell align='left'>
                      {row?.deliveryStatus ? statusMessage('Sent') : statusMessage('Unsent')}
                    </TableCell>
                    <TableCell align='left'>
                      {row?.paymentStatus ? statusMessage('Paid') : statusMessage('Unpaid')}
                    </TableCell>
                    <TableCell align='left' className={classes.actionCell}>
                      <div>
                        <Link to={{pathname: `/edit/${row.id}`, state: row}}>
                          <img src={IEdit} alt='' />
                        </Link>
                      </div>
                      <div>
                        <ModalTool id={row.id} />
                      </div>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </CardContent>
      </Card>
      <Box display='flex' justifyContent='space-between' alignItems='center' className={classes.paginationBtn}>
        <Box alignItems='center' display='flex'>
          <Box className={classes.formControl}>
            <FormControl>
              <Select
                className={classes.selectPerPage}
                value={perPage}
                onChange={handleChangePerPage}
                input={<StylesSelectorInput />}>
                <MenuItem value={5}>5</MenuItem>
                <MenuItem value={10}>10</MenuItem>
                <MenuItem value={15}>15</MenuItem>
              </Select>
            </FormControl>
          </Box>
          <Box className={classes.resultPage}>
            <span>
              Result
              <span>{resultPage()}</span>
            </span>
          </Box>
        </Box>
        <Box>
          <Pagination
            count={count}
            size='large'
            page={page}
            variant='outlined'
            shape='rounded'
            onChange={handleChange}
          />
        </Box>
      </Box>
    </>
  )
}
